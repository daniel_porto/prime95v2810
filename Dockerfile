from ubuntu:16.04
RUN apt-get update && apt-get upgrade -y && apt-get install -y \
    build-essential \
    less git libcurl4-openssl-dev \
&& rm -rf /var/lib/apt/lists/*

ADD . /home/prime95v2810
RUN cd /home/prime95v2810/gwnum && make -f make64
RUN cd /home/prime95v2810/linux64 && make 
ENV PATH /home/prime95v2810/linux64:$PATH

WORKDIR /

